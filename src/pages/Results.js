import React from 'react';
import queryString from 'query-string';
import api from './../utils/api';
import PlayerProfil from './../components/PlayerProfil';
import Loading from "../components/Loading";
import {Link} from 'react-router-dom';

class Results extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            winner: null,
            loser: null
        }
    }

    componentDidMount()
    {
        let players = queryString.parse(this.props.location.search);

        api.battle([
            players.playerOne,
            players.playerTwo
        ]).then((results) => {
            console.log(results);

            this.setState(() => {
                return ({
                    winner: results[0],
                    loser: results[1]
                });
            })
        });
    }

    render() 
    {
        return (
          <div className="result-container">
              {
                  (this.state.winner === null && this.state.loser === null) ?
                      <Loading item="Loading"/>
                      :
                      <section>
                          <div className="clearfix">
                              <h4 className="win-los">Winner</h4>
                              <h4 className="win-los">Loser</h4>
                          </div>
                          <PlayerProfil
                              img={this.state.winner.profile.avatar_url}
                              username={this.state.winner.profile.login}
                          >
                            <p>{this.state.winner.score} points</p>
                          </PlayerProfil>

                          <PlayerProfil
                              img={this.state.loser.profile.avatar_url}
                              username={this.state.loser.profile.login}
                          >
                              <p>{this.state.loser.score} points</p>
                          </PlayerProfil>
                          <Link
                              className="player-form-button"
                              to="/battle"
                          >
                              Restart
                          </Link>
                      </section>
              }
          </div>
        );
    }
}

export default Results;