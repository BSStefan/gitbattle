import React from 'react';
import {NavLink} from 'react-router-dom';


function Nav() {
    return(
        <ul className="nav">
            <li><NavLink exact to="/" activeClassName="nav-active">
                Home
            </NavLink></li>
            <li><NavLink to="/battle" activeClassName="nav-active">
                Battle
            </NavLink></li>
            <li><NavLink to="/popular" activeClassName="nav-active">
                Popular
            </NavLink></li>
        </ul>
    )
}

export default Nav;
