import React from 'react';
import PropTypes from 'prop-types';

class PlayerForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            value: ''
        }
    }

    changeValue(value)
    {
        this.setState(() => {
           return {
              value: value
           };
        });
    }

    findPlayer()
    {
        this.props.findPlayer(this.state.value, this.props.playerName.replace(' ', ''));
    }

    render()
    {
        return(
            <div className="player-form-container">
                <h3>{this.props.playerName}</h3>
                <input
                    type="text"
                    value={this.state.value}
                    onChange={(event) => this.changeValue(event.target.value)}
                    className="player-form-input"
                />
                <button
                    className="player-form-button"
                    onClick={() => this.findPlayer()}
                >
                    Submit
                </button>
            </div>
        );
    }
}

PlayerForm.propTypes = {
  playerName: PropTypes.string.isRequired,
  findPlayer: PropTypes.func.isRequired
};

export default PlayerForm;
