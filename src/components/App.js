import React from 'react';
import Popular from "../pages/Popular";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Nav from './Nav';
import Battle from '../pages/Battle';
import Home from '../pages/Home';
import Results from '../pages/Results';

class App extends React.Component
{

  render()
  {
    return (
      <BrowserRouter>
          <div className="container">
            <Nav/>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/popular" component={Popular}/>
              <Route exact path="/battle" component={Battle}/>
              <Route path="/battle/results" component={Results}/>
              <Route render={() => {
                  return <h1>Not found</h1>
              }}/>
            </Switch>
          </div>
      </BrowserRouter>
    );
  }

}

export default App;
